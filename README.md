# DESAFIO DE DESENVOLVIMENTO #

Criar uma aplicação que englobe todos os requisitos mínimos que serão especificados no decorrer deste readme, utilizando linguagens, tecnologias, bibliotecas, frameworks etc., modernos, pois o objetivo é apenas aprender novas tecnologias.

## Tempo de Desenvolvimento ##
Não há uma data limite, mas seria muito legal se você fosse colocando para os cologuinhas o que você já fez, mostrando tirando dúvidas etc. Custa nada motivar os outros, não é empresa, mas vamos fazer, vai ser legal.

## Requisitos ##

Você deve selecionar uma categoria Backend e uma Frontend:

### Backend ###

Usar tecnologias modernas, preferencialmente com algum framework web alguns exemplos:

- MEAN Stack;
- Go Language;
- Ruby on Rails;
- Python: Tornado, Django (não pode utilizar o administrador para ser seu frontend de cadastro), Flask, Bottle, Web2Py, etc.
- Elixir;
- Kotlin;
- Scala;
- Rust;
- D Language;
- Dart;
- Clojure;
- Outras, converse e sugira antes ;D

### Frontend ###
- Angular 1.x;
- Angular 2.x;
- Vue JS;
- React;
- Entre outras derivações;

Observação: Pode usar tecnologias Javascript a vontade no seu frontend, jQuery sempre vai estar presente, mas tente usar a tecnologia de frontend ao máximo possível. Pode incluir Knockoutjs, Ember, Backbone etc.

## Burocracia ##
Mas o que tenho de fazer? Bem, vamos a parte chata, todos vão aprender algo, então a parte web será obrigatória, a segunda parte também, claro, mas você terá mais liberdade de escolha. Escolha um dos itens a seguir, com muita sabedoria, pois eles farão parte da parte dois do desafio:

* a) Aplicativo Mobile Nativo: Android, iOS e Windows Phone -> Você deve selecionar dois se for fazer nativo, um aplicativo para cada item, exemplo: Android e Windows Phone, Android e iOS ou iOs e Windows Phone;
* b) Aplicativo Mobile Nativo + Híbrido/Híbrido Nativo: Selecionar um único elemento do item a) e criar uma aplicação alternativa a ele usando frameworks, ferramentas etc., tais como: Ionic 1.x, Ionic 2.x, Xamarin, Reactive Native etc.
* c) Aplicativo Mobile Nativo ou Híbrido/Híbrido Nativo e Aplicação Desktop: selecionar um elemento do item a ou b e criar uma aplicação desktop para solucionar o problema, pode ser usando Electron (Node JS), Qt, Wxwidgets, gtk, Clojure Desktop etc.

## Parte Web ##
A parte web será para cadastrar receitas, são basicamente Cruds muito simples, porém você deve ter todo o administrador básico feito em mãos. Os requisitos são:

* Cadastro de usuário;
* Login;
* Cadastro de receitas;
* Editar receita;
* Deletar receita;
* Webservice para listar as receitas de maneira paginada de maneira pública;

Observação: Somente o usuário criador da receita poderá editá-la e/ou apagá-la.

## Parte 2 - Mobile/Híbrido/Desktop ##
* Aplicação deve conter um login com seu modelo web ou social, por exemplo Facebook, Google Plus, Linkedin, Github etc.
* Sua aplicação deve exibir quem é a pessoa conectada e os dados sociais, pode usar quaisquer que sejam, mas o importante é saber: quem sou eu?
* Lista de receitas paginadas;
* Favoritar Receita;
* Exibir detalhes da receita: na paginação mostrar uma lista de receitas com nome da receita, criador e a imagem quando existir, clicou vai para detalhes, imagine em um aplicativo qualquer, por exemplo Buscapé.
* Enviar e-mail para o usuário criador da receita (usar o email fornecido pela sua api)

### Detalhes dos CRUDS ###
- Usuário: Nome, email e senha;
- Receita: Nome, imagem, sobre a receita (espécie de resumo), tempo de preparo, rendimento, ingredientes e como fazer.

### Observação ###
Não será possível cadastrar receitas pelo celular, apenas consumir sua API publica.